package com.company;
import java.awt.*;
import java.io.*;
import java.net.URI;
import java.net.URISyntaxException;
import java.sql.SQLOutput;
import java.io.FileWriter;
import java.util.HashMap;
import java.util.Map;

public class CatalogUtil {
    public static void save(Catalog catalog)
    {
        String filename = catalog.getPath();
        try
        {
            FileOutputStream file = new FileOutputStream(filename);
            ObjectOutputStream out = new ObjectOutputStream(file);

            out.writeObject(catalog);
            out.close();
            file.close();
        }
        catch(IOException exception)
        {
            System.out.println("Eroare la serializare");
        }
    }

    public static Catalog load(String path)
    {
        Catalog catalog = null;
        try
        {
            // Reading the object from a file
            FileInputStream file = new FileInputStream(path);
            ObjectInputStream in = new ObjectInputStream(file);

            // Method for deserialization of object
            catalog = (Catalog)in.readObject();

            in.close();
            file.close();

        }
        catch(IOException ex)
        {
            System.out.println("Eroare la deserializare");
        }
        catch(ClassNotFoundException exception)
        {
            System.out.println("Clasa nu a fost gasita la deserializare");
        }
        return catalog;
    }

    public static void view(Document doc)
    {
        try
        {
            Desktop desktop = null;
            if (Desktop.isDesktopSupported())
            {
                desktop = Desktop.getDesktop();
            }

            FileWriter myWriter = null;
            try
            {
                myWriter = new FileWriter("Document.txt");
                myWriter.write(doc.getId() + "\n" + doc.getName() + "\n" + doc.getLocation() + "\n");

                for (Map.Entry<String, Object> entry : doc.getTags().entrySet())
                {
                    String key = entry.getKey();
                    Object value = entry.getValue();
                    myWriter.write(key + " " + value.toString());
                }

                myWriter.close();
                System.out.println("Successfully wrote to the file.");
            }
            catch (IOException e)
            {
                System.out.println("An error occurred.");
                e.printStackTrace();
            }

            desktop.open(new File("Document.txt"));
            desktop.browse(new URI(doc.getLocation()));
        }
        catch (IOException | URISyntaxException ioe)
        {
            ioe.printStackTrace();
        }
    }
}